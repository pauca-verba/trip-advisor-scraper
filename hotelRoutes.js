/**
 * Created by Aleksandr Volkov on 25/08/16.
 */
// var app = null;
var hotelDB = require('./trip/hotelDB');
var hotel = hotelDB.init();

function init(app) {
	// hotelDB.addSomeHotels();
	// app = selectedApp;
	/*********************************************
	 * hotel routes
	 */
	app.put('/api/hotel', function(req, res){
		console.log(req.body);
		hotelDB.saveHotel(req.body, function(err, data){
			if(!err){
				console.log('saving success');
				res.send({success: true})
			}else{
				res.send({success: false, message: err})
			}
		})
	});
	/**
	 * delete hotel
	 */
	app.delete('/api/hotel/:id', function(req, res){
		console.log('remove', req.params.id);
		hotelDB.removeHotel(req.params.id, function(err, data){
			if(!err){
				res.send({success: true})
			}
		})
	});
	/**
	 * update hotel
	 */
	app.post('/api/hotel/', function(req, res){
		console.log('update', req.body);
		hotelDB.updateHotel(req.body, function(err, data){
			if(!err){
				res.send({success: true})
			}else{
				res.send({success: false, err: err})
			}
		})
	});
	/**
	 * get hotels info by festival id
	 */
	app.get('/api/hotels/:festivalId', function(req, res){
		hotelDB.getHotels(req.params.festivalId, function(data){
			res.send(data)
		})
	});
	/**
	 * get all the hotels
	 */
	app.get('/api/hotels/', function(req, res){
		hotelDB.getHotels(req.params.festivalId, function(data){
			res.send(data)
		})
	});
	/**
	 * get single hotel info
	 */
	app.get('/api/hotel/:id', function(req, res){
		hotelDB.getHotel(req.params.id, function(data){
			res.send(data)
		})
	});
}

module.exports = init;