/**
 * Created by Aleksandr Volkov on 29/08/16.
 */
var festivalActions = Reflux.createActions([
	'getFestivals',
	'removeFestival',
	'doTotalScraping'
]);

var FestivalStore = Reflux.createStore({
	listenables: [festivalActions],
	getFestivals: function () {
		$.ajax({url: '/api/festivals/', dataType: 'json'})
			.then(function (data) {
				this.gotFestivals(data)
			}.bind(this))
	},
	doTotalScraping: function (festivalId, language) {
		$.ajax({url: 'api/scrape-parallel/' + festivalId + '/' + language, dataType: 'json'})
			.then(function (data) {
				this.scrapeComplete(data)
			}.bind(this))
	},
	gotFestivals: function (data) {
		this.trigger('gotFestivals', data)
	},
	scrapeComplete: function (data) {
		this.trigger('scrapeComplete', data)
	}
});
var Link = window.ReactRouter.Link;

var Festivals = React.createClass({
	mixins: [
		Reflux.listenTo(FestivalStore, 'onGotFestivals'),
		Reflux.listenTo(FestivalStore, 'onScrapeComplete'),
	],
	getInitialState: function () {
		return {
			festivals: []
		}
	},
	onGotFestivals: function (event, data) {
		if (event == 'gotFestivals') {

			this.setState({
				festivals: data
			});
		}
		// console.log(event, data);
	},
	onScrapeComplete: function (event, data) {
		// console.log(event, data);
		if (event == 'scrapeComplete') {
			if (data.success) {
				toastr.success('scrape completed')
			} else {
				toastr.error('scraping failed')
			}
		}
	},
	componentWillMount: function () {
		festivalActions.getFestivals();
	},
	doScrape: function (festivalId, language) {
		festivalActions.doTotalScraping(festivalId, language);
	},
	onFestivalRemove: function(festivalId){
		if (window.confirm('remove festival?')) {
			festivalActions.removeFestival(festivalId)
		}
	},
	render: function () {
		var buttonStyles = {
			backgroundColor: 'inherit',
			padding: '1rem',
			cursor: 'pointer'

		};
		var festivals = this.state.festivals.map(function (elem) {
			return <tr key={elem.id}>
				<td>
					<Link to={`festival/${elem.id}`}>{elem.description}</Link>
				</td>
				<td>
					<Link to={`/festival/add`}><i className="glyphicon glyphicon-plus"></i></Link>
				</td>
				<td>
					<Link to={`/festival-upd/${elem.id}`} data-hotel={elem}><i
						className="glyphicon glyphicon-edit"></i></Link>
				</td>
				<td>
					<a className="glyphicon glyphicon-minus "
					   onClick={this.onFestivalRemove.bind(this, elem.id)}></a>
				</td>
				<td>
					<a
						style={buttonStyles} onClick={this.doScrape.bind(this, elem.id, 'en')}>en
					</a>
					<a
						style={buttonStyles} onClick={this.doScrape.bind(this, elem.id, 'es')}>es
					</a>
					<a
						style={buttonStyles} onClick={this.doScrape.bind(this, elem.id, 'fr')}>fr
					</a>
				</td>
			</tr>
		}.bind(this));
		return (
			<div className="container">
				<h4>Festivals</h4>
				<table className="table table-striped table-bordered">
					<thead>
					<tr>
						<td>Festival name</td>
						<td>add</td>
						<td>modify</td>
						<td>remove</td>
						<th>Do festival scraping</th>
					</tr>
					</thead>
					<tbody>
					{festivals}
					</tbody>
				</table>
			</div>
		)
	}
});
window.Festivals = Festivals;