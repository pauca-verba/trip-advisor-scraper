/**
 * Created by Aleksandr Volkov on 23/08/16.
 */


var Router = window.ReactRouter.Router;
var Route = window.ReactRouter.Route;
var Link = window.ReactRouter.Link;
// var History = window.ReactRouter.browserHistory;

var MainMenu = React.createClass({
	getInitialState: function () {
		return {
			menuItems: [
				{url: '', name: 'Festivals'},
				{url: 'hotels', name: 'Hotels'},
			]
		}
	},
	render: function () {
		var links = this.state.menuItems.map(function (elem, index) {
			return <li key={index}>
				 <a href={`/#/${elem.url}`}>{elem.name}</a>
			</li>
		});
		return (
			<div className="container">
				<ul className="nav nav-tabs">
					{links}
				</ul>
			</div>
		);
	}
});
ReactDOM.render((<MainMenu/>), document.getElementById('mainMenu'));
ReactDOM.render((

	<Router>
		<Route path="/" component={Festivals}/>
		<Route path="/reviews/:id" component={Reviews}/>
		<Route path="/festival/:id" component={Hotels}/>
		<Route path="/festival/add" component={AddHotel}/>
		<Route path="/festival-upd/:id" component={AddHotel}/>
		<Route path="/hotels" component={Hotels}/>
		<Route path="/hotel/add" component={AddHotel}/>
		<Route path="/hotel/:id" component={AddHotel}/>
	</Router>
), document.getElementById('routes'));
