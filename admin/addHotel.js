/**
 * Created by Aleksandr Volkov on 24/08/16.
 */
var festivalActions = Reflux.createActions([
	'getFestivals'
]);
var FestivalStore = Reflux.createStore({
	listenables: [festivalActions],
	getFestivals: function(){
		$.ajax({url: '/api/festivals'})
			.then(function(data){
				this.gotFestivals(data)
			}.bind(this), function(err){
				toastr.error(err)
			})
	},
	gotFestivals: function(data){
		this.trigger('festivals', data)
	}
});
var AddHotel = React.createClass({
	mixins: [
		Reflux.listenTo(HotelStore, 'onHotelAdded'),
		Reflux.listenTo(HotelStore, 'onHotelUpdate'),
		Reflux.listenTo(FestivalStore, 'onFestivals')
	],
	getInitialState: function () {
		return {
			hotel: {
				hotelId: '',
				hotelName: '',
				reviewsUrl: ''
			},
			festivals: [
				{value: '', label: ''}
			],
			selectedFestivals:[
				// {value: 'test', label: 'test label'},
				// {value: 'test2', label: 'test label2'},
			]
		}
	},
	onFestivals: function(event, data){
		if(event == 'festivals'){
			this.setState({festivals: data.map(function(elem){
				return {value: elem.id, label: elem.description}
			})})
		}
	},
	componentDidMount: function () {
		console.log(this.props.params);
		if (!!this.props.params.id) {
			hotelActions.getHotel(this.props.params.id);
		}
		festivalActions.getFestivals();
	},
	onHotelAdded: function (event, data) {
		console.log(event);
		if (event == 'hotelAdded') {
			if (data.success) {
				console.log('data success hotelAdded');
				document.location = '/';

			} else {
				console.log(data);
				toastr.error(data.message.message)

			}
		}
	},
	onHotelUpdate: function (event, data) {
		if (event == 'hotelUpdate') {
			var hotel = data;
			var links = hotel.links.map(function(elem){
				return {
					value: elem.festivalId,
					label: this.state.festivals.map(function(fest){
						if(fest.value == elem.festivalId){
							return fest.label;
						}
					})
				}
			}.bind(this));
			this.setState({
				hotel: {
					hotelId: hotel.id,
					hotelName: hotel.description,
					reviewsUrl: hotel.url
				},
				selectedFestivals: links
			});
			// console.log(event, data);
			// document.location = '/';
		}
	},
	handleChange: function (evt) {
		var newHotel = $.extend({}, this.state.hotel);
		newHotel[evt.target.getAttribute('id')] = evt.target.value;
		this.setState({hotel: newHotel})
	},
	onHotelSave: function () {
		var festivals = this.state.selectedFestivals.map(function(elem){
			return {festivalId: elem.value}
		});
		console.log('festivals', festivals);
		if (!this.props.params.id) {
			hotelActions.addHotel({hotel: this.state.hotel, festivals: festivals});
		} else {
			hotelActions.updateHotel({hotel: this.state.hotel, festivals: festivals});

		}
		return false;
	},
	onFestivalChange: function(data){
		console.log(data);
		this.setState({
			selectedFestivals: data
		})
	},
	render: function () {
		function logChange(val) {
			console.log("Selected: ", val);
		}
		return (
			<div className="container">
				<div>
					<div className="form-group">
						<label htmlFor="hotelName">Hotel id</label>
						<input className="form-control"
							   type="text" id="hotelId"
							   onChange={this.handleChange}
							   placeholder="something like d672344 can be found in URL"
							   value={this.state.hotel.hotelId}/>
					</div>
					<div className="form-group">
						<label htmlFor="hotelName">Hotel name</label>
						<input className="form-control" type="text" id="hotelName"
							   onChange={this.handleChange}
							   value={this.state.hotel.hotelName}/>
					</div>
					<div className="form-group">
						<label htmlFor="hotelName">Reviews url</label>
						<input className="form-control"
							   type="url"
							   id="reviewsUrl" required
							   onChange={this.handleChange}
							   value={this.state.hotel.reviewsUrl}/>
					</div>
					<div className="form-group">
						<label htmlFor="festival">Festival</label>
						<Select
							id="festival"
							name="form-field-name"
							value={this.state.selectedFestivals}
							options={this.state.festivals}
							onChange={this.onFestivalChange}
							multi={true}
						/>
					</div>
					<div className="form-group">
						<button className="btn" onClick={this.onHotelSave}>Save</button>
					</div>
				</div>
			</div>
		);
	}
});

window.AddHotel = AddHotel;
