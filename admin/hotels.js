/**
 * Created by Aleksandr Volkov on 23/08/16.
 */
var hotelActions = Reflux.createActions([
	'getHotels',
	'scrapeHotel',
	'addHotel',
	'removeHotel',
	'getHotel',
	'updateHotel'

]);
var Router = window.ReactRouter.Router;
var Route = window.ReactRouter.Route;
var Link = window.ReactRouter.Link;
// var host = 'http://localhost:8002';
var host = '';

var HotelStore = Reflux.createStore({
	listenables: [hotelActions],
	getHotel: function (hotelId) {
		$.ajax({url: host + '/api/hotel/' + hotelId, dataType: 'json'})
			.then(function (data) {
				this.hotelUpdate(data)
			}.bind(this))
	},
	getHotels: function (festivalId) {
		if(!festivalId){
			festivalId = '';
		}
		console.log('doing query');
		$.ajax({url: host + '/api/hotels/' + festivalId, dataType: 'json'})
			.then(function (data) {
				this.hotelsUpdate(data)
			}.bind(this))
	},
	removeHotel: function (hotelId) {
		$.ajax({url: host + '/api/hotel/' + hotelId, dataType: 'json', method: 'DELETE'})
			.then(function (data) {
				this.getHotels();
			}.bind(this))
	},
	addHotel: function (data) {
		console.log(data);
		$.ajax({
			url: host + '/api/hotel',
			data: {
				id: data.hotel.hotelId,
				url: data.hotel.reviewsUrl,
				description: data.hotel.hotelName,
				festivals: data.festivals
			}, method: 'PUT', dataType: 'json'
		})
			.then(function (resp) {
				console.log('got hotel add resp', resp);
				this.onHotelAdded(resp);
			}.bind(this))
	},
	updateHotel: function (data) {
		$.ajax({
			url: host + '/api/hotel',
			data: {
				id: data.hotel.hotelId,
				url: data.hotel.reviewsUrl,
				description: data.hotel.hotelName,
				festivals: data.festivals
			}, method: 'POST', dataType: 'json'
		})
			.then(function (resp) {
				this.onHotelAdded(resp);
			}.bind(this))
	},
	scrapeHotel: function (hotelId, language) {
		$.ajax({url: host + '/api/scrape/' + hotelId + '/' + language})
			.then(function (data) {
				// console.log(data);
				toastr.success('scraping succeed');
				this.reviewsCountUpdate({
					hotelId: hotelId,
					reviewsCount: data.recordsSaved
				})
			}.bind(this), function () {
				toastr.error('scraping error')
			})
	},
	reviewsCountUpdate: function (data) {
		console.log('trigger reviewsSaved', data);
		this.trigger('reviewsSaved', data);
	},
	hotelsUpdate: function (data) {
		console.log('trigger hotelsUpdate');
		this.trigger('hotelsUpdate', data);
	},
	hotelUpdate: function (data) {
		console.log('trigger single hotel Update');
		this.trigger('hotelUpdate', data);
	},
	onHotelAdded: function (data) {
		this.trigger('hotelAdded', data)
	}
});

var Hotels = React.createClass({
	mixins: [
		Reflux.listenTo(HotelStore, 'onHotelsUpdate'),
		Reflux.listenTo(HotelStore, 'onReviewsSaved'),
	],
	getInitialState: function () {
			console.log('hotels init');
		return {
			hotels: [
				{
					description: '',
					url: '',
					id: 0
				}
			],
			scrapingEnabled: true
		};
	},
	componentDidMount: function () {
		// console.log(this.props.params);
	},
	componentWillMount: function () {
		console.log('getting hotels');
		hotelActions.getHotels(this.props.params.id);
	},
	onHotelsUpdate: function (event, data) {
		if (event == 'hotelsUpdate') {
			console.log(event);
			console.log('onHotelsUpdate', data, event);
			this.setState({hotels: data})
		}
	},
	onReviewsSaved: function (event, data) {
		if (event == 'reviewsSaved') {
			var newState = this.state.hotels.slice();
			var selectedHotel = _.find(newState, function (elem) {
				return elem.id == data.hotelId
			});
			selectedHotel.reviewsCount = data.reviewsCount;
			this.setState({hotels: newState, scrapingEnabled: true});
			// console.log('onReviewsSaved', this.state, data, event);

		}
	},
	onHotelRemove: function (hotelId) {
		if (window.confirm('remove hotel?')) {
			hotelActions.removeHotel(hotelId)
		}
	},
	doScrape: function (hotelId, language) {
		console.log('do scrape', hotelId);
		this.setState({scrapingEnabled: false});
		hotelActions.scrapeHotel(hotelId, language);
		return false;
	},
	render: function () {
		var buttonStyles = {
			backgroundColor: 'inherit',
			padding: '1rem',
			cursor: 'pointer',
			textDecoration:'none'
		};
		var hotels = this.state.hotels.map(function (hotel, index) {
			return <tr key={index}>
				<td >
					<Link to={`/reviews/${hotel.id}`}>{hotel.description}</Link>
				</td>
				<td >
					{hotel.rating}
				</td>
				<td >
					{hotel.reviewsCount}
				</td>
				<td>
					<Link to={`/hotel/add`}><i className="glyphicon glyphicon-plus"></i></Link>
				</td>
				<td>
					<Link to={`/hotel/${hotel.id}`} data-hotel={hotel}><i
						className="glyphicon glyphicon-edit"></i></Link>
				</td>
				<td>
					<a className="glyphicon glyphicon-minus "
					   onClick={this.onHotelRemove.bind(this, hotel.id)}></a>
				</td>
				<td >
					<a onClick={this.doScrape.bind(this, hotel.id, 'en')}
							style={buttonStyles} disabled={!this.state.scrapingEnabled}>En
					</a>
					<a onClick={this.doScrape.bind(this, hotel.id, 'es')}
					   style={buttonStyles} disabled={!this.state.scrapingEnabled}>Es
					</a>
					<a onClick={this.doScrape.bind(this, hotel.id, 'fr')}
					   style={buttonStyles} disabled={!this.state.scrapingEnabled}>Fr
					</a>
				</td>
			</tr>
		}.bind(this));
		return (
			<div className="container">
				<h4>Hotels</h4>
				<table className="table table-striped table-bordered">
					<thead>
					<tr>
						<th className="col-md-5">Hotel</th>
						<th className="col-md-2">Rating</th>
						<th className="col-md-2">Number of reviews</th>
						<th className="col-md-1"><Link to={`/hotel/add`}>Add hotel</Link></th>
						<th className="col-md-1"></th>
						<th className="col-md-1"></th>
						<th className="col-md-2">Do scraping</th>
					</tr>
					</thead>
					<tbody>
					{hotels}

					</tbody>
				</table>

			</div>
		)
	}
});
window.Hotels = Hotels;
window.HotelStore = HotelStore;
window.hotelActions = hotelActions;

// ReactDOM.render(<Hotels/>, document.getElementById('hotels'));