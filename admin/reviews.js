/**
 * Created by Aleksandr Volkov on 23/08/16.
 */
var Actions = Reflux.createActions([
	'getHotels',
	'scrapeHotel',
	'getReviews',

]);
var host = '';

var TripStore = Reflux.createStore({
	listenables: [Actions],
	getReviews: function (hotelId) {
		$.ajax({url: host + '/api/get-hotel-reviews/' + hotelId, dataType: 'json'})
			.then(function (data) {
				this.reviewsUpdate(data)
			}.bind(this))
	},
	reviewsUpdate: function (data) {
		this.trigger('reviewsChange', data);
	}
});

var Reviews = React.createClass({
	mixins: [Reflux.listenTo(TripStore, 'onReviewsChange')],
	onReviewsChange: function (event, data) {
		this.setState({reviews: data})
	},
	getInitialState: function () {
		return {
			reviews: [{
				id: 1,
				description: '',
				hotelId: '',
				userName: '',
				fullReviewUrl: '',
				stars: '',
				isMoreDataAvailable: false
			}]
		};
	},
	componentWillMount: function () {
		console.log(this.props.params.id);
		Actions.getReviews(this.props.params.id)
	},
	render: function () {
		var reviews = this.state.reviews.map(function (review, index) {
			return <Review reviewData={review} key={index}/>
		});
		return (
			<div className="container">
				<table className="table table-striped table-bordered">
					<ReviewHeader/>
					<tbody>
					{reviews}
					</tbody>
				</table>
			</div>
		)
	}

});
var ReviewHeader = React.createClass({
	render: function () {
		return (
			<thead>
			<tr className="row">
				<th className="col-md-1">Language</th>
				<th className="col-md-1">Review id</th>
				<th className="col-md-1">Hotel id</th>
				<th className="col-md-1">Title</th>
				<th className="col-md-3">Description</th>
				<th className="col-md-2">User name</th>
				<th className="col-md-2">Review url</th>
				<th className="col-md-1">Stars</th>
				{/*<div className="col-md-1">Is more data available</div>*/}
			</tr>
			</thead>

		)
	}
});
var Review = React.createClass({
	render: function () {
		var review = this.props.reviewData;
		return (
			<tr className="row">
				<td className="col-md-1">{review.language}</td>
				<td className="col-md-1">{review.reviewId}</td>
				<td className="col-md-1">{review.hotelId}</td>
				<td className="col-md-1">{review.title}</td>
				<td className="col-md-3">{review.description}</td>
				<td className="col-md-2">{review.userName}</td>
				<td className="col-md-2">{review.fullReviewUrl}</td>
				<td className="col-md-1">{review.stars}</td>
				{/*<div className="col-md-1">{review.isMoreDataAvailable}</div>*/}
			</tr>
		)
	}
});

window.Reviews = Reviews;


// ReactDOM.render(<Reviews/>, document.getElementById('reviews'));