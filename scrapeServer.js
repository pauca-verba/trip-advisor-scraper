/**
 * Created by Aleksandr Volkov on 22/08/16.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var reviewDB = require('./trip/reviewDB');
var review  = reviewDB.dbInit();

var hotelDB = require('./trip/hotelDB');
var hotel = hotelDB.init();

var festivalHotelLinkDB = require('./trip/festivalHotelLinkDB.js');
var auth = require('basic-auth-connect');

var festivalDB = require('./trip/festivalDB');
festivalDB.init();
// var scrape = require('./trip/tripScrape.js');
var Scrape = require('./trip/reviewScrape.js');
var ParallelScraping = require('./trip/parallelScraping');


// scrape.init(review);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(auth('alex', 'pass'));
/**
 * TODO
 * create a route for dummy init
 */
var hotelRoutes = require('./hotelRoutes.js')(app);
app.get('/api/dummy', function(req, res){
	scrape.insertDummies(function(){
		res.send('ok, will do what i can')
	})
});
// routes.init(app);
/*********************************************
 * scraping routes
 */
app.get('/api/scrape/:id/:language', function(req, res){
	// console.log(req);
	var language = req.params.language || 'en';
	var scrape = new Scrape({
		language: language
	});
	scrape.doScraping(req.params.id, function(recordsSaved){
		res.send({recordsSaved: recordsSaved, success: true})
	});
});
/**
 * parallel scraping
 */
app.get('/api/scrape-parallel/:festivalId/:language', function(req, res){
	// console.log(req);
	console.log('req.params.language', req.params.language);
	var language = req.params.language || 'en';
	var scrape = new ParallelScraping({
		language: language
	});
	scrape.scrapeByFestival(req.params.festivalId, language, function(err, recordsSaved){
		console.log('final procedure', recordsSaved);
		res.send({recordsSaved: recordsSaved, success: true})
	});
});


/*********************************************
 * reviews routes
 */
// app.get('/api/get-hotel-reviews/:id', function(req, res){
// 	console.log('hotel id', req.params.id);
// 	reviewDB.getHotelReviews(req.params.id, function(data){
// 		res.send(data);
// 	})
// });

/*********************************************
 * it should check for hotel existence if it does not exists:
 * - save to DB
 * - do scraping
 * - return data
 */
app.post('/api/get-hotel-reviews/:id/:language', function(req, res){
	var hotelId = req.params.id;
	var language = req.params.language;
	var url = req.body.url;

	var parallelScraping = new ParallelScraping();
	parallelScraping.getHotelReviews(hotelId, language, url, res);
});
/**
 * get festivals data
 */
app.get('/api/festivals', function(req, res){
	festivalDB.getFestivals(function(err, data){
		if(!err){
			res.send(data)
		}
	})
});
/**
 * update festival
 */
app.post('/api/festival', function(req, res){
	festivalDB.updateFestival(req.body, function(err, data){
		if(!err){
			console.log('saving success');
			res.send({success: true})
		}else{
			res.send({success: false, message: err})
		}
	})
});
/**
 * remove festival
 */
app.delete('/api/festival/:id', function(req, res){
	console.log('remove', req.params.id);
	festivalDB.removeFestival(req.params.id, function(err, data){
		if(!err){
			res.send({success: true})
		}
	})
});
/**
 * add festival
 */
app.put('/api/festival', function(req, res){
	festivalDB.createFestival(req.body, function(err, data){
		if(!err){
			console.log('saving success');
			res.send({success: true})
		}else{
			res.send({success: false, message: err})
		}
	})
});
/**
 * festival to hotel links
 *
 */
// app.put('/api/save-links', function(req, res){
// 	festivalHotelLinkDB.saveLinks(req.body, function(err, data){
// 		if(!err){
// 			res.send(data)
// 		}else{
// 			res.send({success: false})
// 		}
// 	})
// });
app.use('/', express.static('./admin'));
app.use('/bower_components', express.static('./bower_components'));
app.use('*', function(req, res){
	res.redirect('/')
});
app.listen(8002, function(err){
	// scrape.init();
	if(err){
		console.log(err);
	}else{
		console.log('listen on 8002');
	}
});