/**
 * Created by Aleksandr Volkov on 05/09/16.
 */
var expect = require('chai').expect;
var ParallelScraping = require('../trip/parallelScraping');
var stubs = require('./stubs.js');
var hotelDB = require('../trip/hotelDB');
var hotel = hotelDB.init();
var hotelId = 'd93614';
var HotelsUpdDB = require('../trip/hotelsUpdDB.js');
var hotelsUpdDB = new HotelsUpdDB();
var testUrl = 'https://www.tripadvisor.com/Hotel_Review-g60763-d93614-Reviews-The_St_Regis_New_York-New_York_City_New_York.html';
describe('scraping of several hotels', function () {
	it('should scrape all the records for the ', function (done) {
		this.timeout(12000);
		var scrape = new ParallelScraping({
			language: 'en',
			request: stubs.getFakeReq(),
			recordsLimit: 20
		});
		scrape.scrapeByFestival(3, 'en', function (err, data) {
			// console.log(data);
			done()
		})
	});
	it('should do scraping and return results from the hotels table', function(done){
		var res = {
			send: function(data){
				console.log(data.length);
				expect(data.length).to.be.equal(20);
				done()
			}
		};
		var scrape = new ParallelScraping({
			language: 'en',
			request: stubs.getFakeReq(),
			recordsLimit: 20
		});
		scrape.scrapeAndGetReview(hotelId, 'en', res)
	});
	it('should start new scraping session if hotel is not found', function(done){
		var res = {
			send: function(data){
				console.log(data.length);
				// expect(data.length).to.be.equal(20);
				done()
			}
		};
		var scrape = new ParallelScraping({
			language: 'en',
			request: stubs.getFakeReq(),
			recordsLimit: 20
		});
		scrape.getHotelReviews(hotelId, 'en', testUrl, res)
	});
	it('should start new scraping session if hotel is not found', function(done){
		var res = {
			send: function(data){
				expect(data.length).to.be.equal(20);
				console.log('should start new scraping session if hotel is not found');
				done()
			}
		};
		// we need to remove time of the last update
		var scrape = new ParallelScraping({
			language: 'en',
			request: stubs.getFakeReq(),
			recordsLimit: 20
		});
		hotelDB.removeHotel(hotelId, function(){
			scrape.getHotelReviews(hotelId, 'en', testUrl, res)
		})
	});
	it('should start new scraping session if Upd record is not found', function(done){
		var res = {
			send: function(data){
				expect(data.length).to.be.equal(20);
				console.log('should start new scraping session if Upd record is not found');
				done()
			}
		};
		// we need to remove time of the last update
		var scrape = new ParallelScraping({
			language: 'en',
			request: stubs.getFakeReq(),
			recordsLimit: 20
		});
		hotelsUpdDB.remove(hotelId, 'en');
		scrape.getHotelReviews(hotelId, 'en', testUrl, res)
	});
});