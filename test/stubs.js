/**
 * Created by Aleksandr Volkov on 05/09/16.
 */
var ScrapeReview = require('../trip/reviewScrape.js');
var fs = require('fs');
var stubs = {
	getReviewModule: getReviewModule,
	getFakeReq: getFakeReq
};
function getReviewModule(useErrors) {
	/**
	 * we need to mock Request module so it can return file instead of doing real request
	 * @param params
	 * @param cb
	 */
	var reqFake = getFakeReq(useErrors);
	var params = {
		request: reqFake,
		recordsLimit: 20,
		language: 'en'
	};
	return new ScrapeReview(params);
}

function getFakeReq(useErrors) {
	return function (params, cb) {
		var index = params.url.indexOf('-or10-') > -1 ? 1 : 0;
		// console.log('reading file', params.url, index);
		if(typeof params.url == 'undefined'){
			useErrors = true;
		}
		var id = params.url.match(/-d\d+-/ig)[0].replace(/-/ig, '');
		cb(
			(useErrors ? new Error('something wrong') : null),
			null,
			fs.readFileSync('test/fixtures/' + id + '_' + index + '.html', 'utf8'))
	};
}
module.exports = stubs;