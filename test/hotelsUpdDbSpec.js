/**
 * Created by Aleksandr Volkov on 02/09/16.
 */
'use strict';

var expect = require('chai').expect;
var HotelsUpdDB = require('../trip/hotelsUpdDB.js');
var hotelsUpd;
var hotelId = 123;
var language = 'en';
const month = 1000 * 60 * 60 * 24 * 30;

hotelsUpd = new HotelsUpdDB();

beforeEach('cleaning records from db', function (done) {

	hotelsUpd.remove(hotelId, language)
		.then(function () {
			done();
		})

});

after(function (done) {
	hotelsUpd.remove(hotelId, language)
		.then(function () {
			done();
		})
});

describe.skip('init stuff', function () {
	it('should have a new record with hotel and language', function (done) {

		// date of update about a month ago
		hotelsUpd.add({
			hotelId: hotelId,
			language: language,
			updDate: new Date() - month
		}).then(function (data) {
			// console.log('data added', data.length);
			hotelsUpd.getUpd(hotelId, language)
				.then(function (data) {
					expect(data).to.have.property('language');
					expect(data.language).to.be.equal('en');
					expect(data).to.have.property('hotelId');
					expect(data.hotelId).to.be.equal('123');
					done();
				})
		})

	});
	it('should say that i need to do scraping', function (done) {
		// date of update about a month ago
		hotelsUpd.add({
			hotelId: hotelId,
			language: language,
			updDate: ((new Date()) - month)
		}).then(function (data) {
			hotelsUpd.getUpd(hotelId, language)
				.then(function (data) {
					hotelsUpd.isTimeToScrape(hotelId, language)
						.then(function (data) {
							expect(data.isUpdateNeeded).to.be.equal(true);
							done();
						});j
				})
		})
	});
	it('should say that i don\'t need to do scraping', function (done) {
		// date of update about a month ago
		hotelsUpd.add({
			hotelId: hotelId,
			language: language,
			updDate: (new Date() - month / 2)
		}).then(function (data) {
			// console.log('data added', data.length);
			hotelsUpd.getUpd(hotelId, language)
				.then(function (data) {
					hotelsUpd.isTimeToScrape(hotelId, language)
						.then(function (data) {
							expect(data.isUpdateNeeded).to.be.equal(false);
							done();
						});
				})
		})
	});
	it('should say that i need to do scraping: case with no records', function (done) {
		// date of update about a month ago
		// console.log('data added', data.length);
		hotelsUpd.getUpd(hotelId, language)
			.then(function (data) {
				hotelsUpd.isTimeToScrape(hotelId, language)
					.then(function (data) {
						expect(data.isUpdateNeeded).to.be.equal(true);
						done();
					});
			})
	});
	it('should update scraping date and tells that up ', function (done) {
		// date of update about a month ago
		var twoWeeksAgo = (new Date() - month / 2);
		hotelsUpd.add({
			hotelId: hotelId,
			language: language,
			updDate: twoWeeksAgo
		}).then(function (data) {
			expect(data.updDate.getTime()).to.be.equal(twoWeeksAgo);
			var currentDate = new Date();
			hotelsUpd.upd(hotelId, language, currentDate)
				.then(function (data) {
					hotelsUpd.getUpd(hotelId, language)
						.then(function (data) {
							expect(data.updDate.getTime()).to.be.equal(currentDate.getTime());
							done();

						})

				});
		})
	});
});
