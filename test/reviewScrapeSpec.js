/**
 * Created by Aleksandr Volkov on 03/09/16.
 */
var Scrape = require('../trip/reviewScrape.js');
var expect = require('chai').expect;
var cheerio = require('cheerio');
var reviewDB = require('../trip/reviewDB.js');
var stubs = require('./stubs.js');
reviewDB.dbInit();

var testUrl = 'https://www.tripadvisor.com/Hotel_Review-g60763-d93614-Reviews-The_St_Regis_New_York-New_York_City_New_York.html';
var hotelId = 'd93614';

describe.skip('scraping one page', function () {
	var scrape = stubs.getReviewModule();
	it('should scrape one page', function (done) {
		scrape.scrapeOne(testUrl)
			.then(function (data) {
				// console.log('data', data.scrapedRecords);
				expect(data.scrapedRecords).to.be.equal(10);
				// console.log(scrape.hotelName);
				expect(scrape.hotelName.trim()).to.be.equal('The St. Regis New York');
				expect(scrape.hotelId).to.be.equal(hotelId);
				done();
			}, function (err) {
				console.log(err);
			})

	});
	it('should return error message on err', function (done) {
		var scrape = stubs.getReviewModule(true);

		scrape.scrapeOne(testUrl)
			.then(function (data) {

			}, function (err) {
				expect(err.message).to.be.equal('something wrong');
				done()
			})

	})
});

describe.skip('scraping many pages', function () {
	it('should get offsets from the hotel page', function (done) {
		var scrape = stubs.getReviewModule();
		scrape.request({
			url: testUrl
		}, function (err, resp, body) {
			expect(scrape.offsets.length).to.be.equal(0);
			scrape.getOffsets(cheerio.load(body));
			// 7 is the number of links from our stub
			expect(scrape.offsets.length).to.be.equal(7);
			done();
		});
	});
	// this.timeout(10000);
	it('should save data for the hotel on the hotel till the limit', function (done) {
		reviewDB.clearHotelRecords(hotelId, 'en');
		var scrape = stubs.getReviewModule();
		scrape.offsets = [{offset: '0', link: undefined},
			{
				offset: '10',
				link: '/Hotel_Review-g60763-d93614-Reviews-or10-The_St_Regis_New_York-New_York_City_New_York.html#REVIEWS'
			},
			{
				offset: '20',
				link: '/Hotel_Review-g60763-d93614-Reviews-or20-The_St_Regis_New_York-New_York_City_New_York.html#REVIEWS'
			}];
		scrape.scrapedRecords.length = 10;
		// scrape.recordsSaved = 10;
		scrape.iteration = 1;
		scrape.scrapeMany(hotelId, function (data) {
			expect(data.length).to.be.equal(20);
			reviewDB.getHotelReviews(hotelId, 'en', function (data) {
				expect(data.length).to.be.equal(10);
				done();

			});
		})

	});
	it('should doScraping function', function (done) {
		var scrape = stubs.getReviewModule();
		scrape.doScraping(hotelId, function(data){
			expect(data.length).to.be.equal(20);
			expect(data[0]).to.have.property('title');
			expect(data[0]).to.have.property('stars');
			expect(data[0]).to.have.property('reviewId');
			expect(data[0]).to.have.property('cityId');
			done();

		})
	})
});

