/**
 * Created by Aleksandr Volkov on 03/09/16.
 */
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var reviewDB = require('./reviewDB.js');
var hotelDB = require('./hotelDB');
hotelDB.init();
var festivalHotelLinkDB = require('./festivalHotelLinkDB');
var festivalsDB = require('./festivalDB');


var ScrapeReview = function (params) {
	if (!params) params = {};
	this.review = null;
	this.festivalId = params.festivalId || null;
	this.request = params.request || request;
	this.mainHost = 'https://www.tripadvisor.com';
	this.hotelId = params.hotelId || null;
	this.hotelUrl = null;
	this.cityId = null;
	this.language = params.language;
	this.recordsLimit = params.recordsLimit || 30;
	this.review = reviewDB.dbInit();
	this.recordsSaved = 0;
	this.hotelIDs = [];
	this.scrapedRecords = [];
	this.languages = {
		en: 'com',
		es: 'es',
		fr: 'fr',
	};
	this.offsets = [];
	this.iteration = 0;
};
/**
 * parsing of the page
 * @param $partialReview
 * @param res
 */
ScrapeReview.prototype.processPage = function ($partialReview) {
	$partialReview.each(function (index, elem) {
		try {
			// we can get the amount of stars from the name of the class, i.e. s40
			var stars = cheerio(elem).find('.sprite-rating_s_fill.rating_s_fill').attr('class')
				.replace('sprite-rating_s_fill', '')
				.replace('rating_s_fill', '')
				.replace(/\s*/ig, '')
				.replace('s', '');
			// we need to figure out is there more data available and strip off whitespaces and garbage
			var isMoreDataAvailable = false;
			var description = cheerio(elem).find('.entry .partial_entry');
			var addMore = cheerio(description).find('.partnerRvw');
			if (typeof addMore[0] != 'undefined') {
				isMoreDataAvailable = true;
				cheerio(description).find('.partnerRvw').remove();
			}
			description = description.text().replace(/^\s+|\s+$/ig, '');
			// console.log('hotelData', hotelData);
			this.scrapedRecords.push({
				reviewId: cheerio(elem).attr('id').match(/\d+/ig)[0],
				title: cheerio(elem).find('.noQuotes').text(),
				description: description,
				hotelId: this.hotelId,
				cityId: this.cityId,
				userImage: cheerio(elem).find('.avatar img').attr('src'),
				userName: cheerio(elem).find('.username span').text(),
				userLocation: cheerio(elem).find('.location').text(),
				ratingDate: cheerio(elem).find('.ratingDate').text(),
				fullReviewUrl: cheerio(elem).find('.quote a').attr('href'),
				stars: stars,
				isMoreDataAvailable: isMoreDataAvailable,
				language: this.language
			});

		} catch (err) {
			console.log(err);
		}
	}.bind(this));
};
/**
 * here we defining Ids of Hotel and City
 * @param url
 */

ScrapeReview.prototype.setHotelUrl = function (url) {
	try {
		this.hotelId = url.match(/-d[0-9]*-/ig)[0].replace(/-/g, '');
		this.cityId = url.match(/-g[0-9]*-/ig)[0].replace(/-/g, '');
	} catch (err) {
		console.log(err);
	}
};
/**
 * scrapes one page of reviews for specified hotel
 * @param url of the hotel
 */
ScrapeReview.prototype.scrapeOne = function (url) {

	return new Promise(function (success, fail) {
		this.request({url: url, method: 'GET'},
			function (err, resp, body) {
				// console.log(url);
				if (err) {
					console.log(err);
					fail(err);
					return false;
				}
				var $ = cheerio.load(body);
				try {
					var $body = $('.reviewSelector');
					this.setHotelUrl(url);
					if(!this.offsets.length){
						// console.log(this.iteration, url);
						this.getOffsets($);
					}
					// console.log(this.hotelId + '_' + this.iteration + '.html');
					// fs.writeFileSync(this.hotelId + '_' + this.iteration + '.html', body);
					if (!this.hotelName) {
						this.setHotelRating(this.hotelId, $);
					}
					this.processPage($body);
					success({
						scrapedRecords: this.scrapedRecords.length,
						body: $
					});
				} catch (err) {
					fail(err)
				}
			}.bind(this));
	}.bind(this))
};
/**
 * scrapes rest of the records
 * scraping many records up to the specified limit
 */
ScrapeReview.prototype.scrapeMany = function (hotelId, cb) {
	if (this.offsets.length) {
		if ((this.scrapedRecords.length < this.recordsLimit) && (this.offsets.length > this.iteration)) {
			// console.log('offset', this.iteration, this.offsets[this.iteration].link);
			this.scrapeOne(this.getDomainForLanguage(this.language) + this.offsets[this.iteration].link)
				.then(function (data) {
					this.iteration++;
					this.scrapeMany(hotelId, cb);
				}.bind(this))
		} else {
			reviewDB.saveReviews(this.scrapedRecords)
				.then(function (data) {
						console.log('all saved ' + hotelId, this.scrapedRecords.length);
						// hotelDB.updateReviewsCount(hotelId, reviewDB.getRecordsSaved());
						cb(this.scrapedRecords)
					}.bind(this),
					function (err) {
						console.log('save err', err);
						cb(err)
					});
		}
	} else {
		cb('offset is not set')
	}
};
/**
 * we need to do scraping for several languages, this is a way to define domain for it
 * @param language
 * @returns {*}
 */
ScrapeReview.prototype.getDomainForLanguage = function (language) {
	var domain;
	if (language != 'en') {
		domain = this.mainHost.replace('.com', '.' + this.languages[language] + '')
	} else {
		domain = this.mainHost
	}
	// console.log(domain);
	return domain;
};

/**
 * here we getting url of hotels and start scraping process
 * @param hotelId
 * @param language
 * @param cb
 */
ScrapeReview.prototype.doScraping = function (hotelId, cb) {
	console.log('hotelId', hotelId);
	var recordsSaved = 0;
	// var offsets = [];
	// we need url of reviews page and we assume that it has a .com domain
	hotelDB.getHotelUrl(hotelId)
		.then(function (data) {
			if (this.language != 'en') {
				data.url = data.url.replace('.com/', '.' + this.languages[this.language] + '/')
			}
			// console.log(data.url);
			if (!!data && !!data.url) {
				// var recordsSaved = 0;
				// clearing old reviews
				reviewDB
					.clearHotelRecords(hotelId, this.language)
					.then(function (deleted) {
						// console.log('deleted', deleted);
						this.scrapeOne(data.url)
							.then(function (data) {
								this.setHotelRating(hotelId, data.body);
								if (this.offsets.length) {
									this.iteration++;
									this.scrapeMany(hotelId, cb);

								} else {
									// we have only single page with reviews
									hotelDB.updateReviewsCount(hotelId, data.scrapedRecords.length);
									cb(data.scrapedRecords);
								}
								// console.log('new rec saved', data.recordsSaved, 'offsets:', offsets.length);
							}.bind(this))

					}.bind(this))
			} else {
				console.log('no url found for ID:', hotelId);
			}
		}.bind(this))


};

/**
 * there are different pages for reviews, we need to get their names
 * @param $
 */
ScrapeReview.prototype.getOffsets = function ($) {
	try {
		var pager = $('.pageNumbers .pageNum');
		pager.each(function (index, elem) {
			this.offsets.push({
				offset: cheerio(elem).attr('data-offset'),
				link: cheerio(elem).attr('href')
			})
		}.bind(this));
		// console.log('offsets.length', offsets.length);
		// console.log(this.offsets);
	} catch (err) {
		console.log(err);
	}
};
/**
 * here we getting hotel rating and hotel name
 * @param id
 * @param $
 */
ScrapeReview.prototype.setHotelRating = function (id, $) {
	this.hotelStars = $('.rating_rr img').attr('content');
	this.hotelName = $('#HEADING').text();
	// console.log('hotelName', hotelName, hotelStars);
	hotelDB.updateHotel({
		id: id,
		rating: this.hotelStars,
		description: this.hotelName
	})
};
module.exports = ScrapeReview;