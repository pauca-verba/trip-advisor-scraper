/**
 * Created by Aleksandr Volkov on 05/09/16.
 */
var ScrapeReview = require('./reviewScrape.js');
var async = require('async');
var festivalHotelLinkDB = require('./festivalHotelLinkDB');
var HotelsUpdDB = require('./hotelsUpdDB.js');
var hotelsUpdDB = new HotelsUpdDB();
var reviewDB = require('./reviewDB.js');
reviewDB.dbInit();
// var request = require('request');
festivalHotelLinkDB.init();
var hotelDB = require('./hotelDB');
var hotel = hotelDB.init();

function ParallelScraping(params) {
	if(!params){
		params = {}
	}
	this.request = params.request || null;
	this.recordsLimit = params.recordsLimit;
}

ParallelScraping.prototype.scrapeByFestival = function (festivalId, language, final) {
	var arr = [];
	festivalHotelLinkDB.getLinksByFestival(festivalId, function (err, data) {
		// console.log('getLinksByFestival', data);
		arr = data.map(function (elem) {
			return elem.hotelId;
		});
		// console.log(arr);
		var scrapes2 = [];
		var that = this;
		arr.forEach(function (hotelId) {
			scrapes2.push(function (cb) {
				console.log(hotelId);
				var params = {
					language: language,
				};
				if (that.request) {
					params.request = that.request;
				}
				if (that.recordsLimit) {
					params.recordsLimit = that.recordsLimit
				}
				// console.log(params);
				var scrape = new ScrapeReview(params);
				scrape.doScraping(hotelId, function (data) {
					// console.log('hotel id', hotelId);
					console.log('do scraping callback');
					cb(null, data)
				});
				// })(hotelId)
			})
		});

		async.parallel(scrapes2, function (err, res) {
			console.log('getting to final');
			final(err, res);
			// console.log(res);
		})
	}.bind(this));
};
ParallelScraping.prototype.scrapeAndGetReview = function (hotelId, language, res) {
	var params = {
		language: language
	};
	if (!!this.request) {
		params.request = this.request
	}
	if (this.recordsLimit) {
		params.recordsLimit = this.recordsLimit
	}
	var scrape = new ScrapeReview(params);
	scrape.doScraping(hotelId, function () {
		hotelsUpdDB.remove(hotelId, language);
		hotelsUpdDB.add({hotelId: hotelId, language: language, updDate: new Date()});
		// sending newly scraped data
		reviewDB.getHotelReviews(hotelId, language, function (data) {
			res.send(data);
		})
	});
};
ParallelScraping.prototype.getHotelReviews = function (hotelId, language, url, res) {
	hotelDB.getHotel(hotelId, function (data) {
		// hotel not found
		if (!data) {
			console.log('hotel not found');
			// no hotel found
			hotelDB.saveHotel({
				url: url
			}, function () {
				console.log('saving new hotel');
				// now hotel saved, we can do scraping
				this.scrapeAndGetReview(hotelId, language, res);
			}.bind(this))
		} else {
			// hotel exists need to check when it was updated last time
			console.log('hotel exists need to check when it was updated last time');
			hotelsUpdDB.isTimeToScrape(hotelId, language).then(function(data){
				if(data.isUpdateNeeded){
					console.log('it is time to scrape');
					this.scrapeAndGetReview(hotelId, language, res)
				} else {
					console.log('last scrape is fresh enough');
					// hotel exists, return reviews
					reviewDB.getHotelReviews(hotelId, language, function (data) {
						if (data.length) {
							res.send(data);
						} else {
							this.scrapeAndGetReview(hotelId, language, res);
						}
					}.bind(this))
				}
			}.bind(this));

		}
	}.bind(this))
};

module.exports = ParallelScraping;
