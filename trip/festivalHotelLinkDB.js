/**
 * Created by Aleksandr Volkov on 25/08/16.
 */
var Sequelize = require('sequelize');
var sequelize;
var DBconnect = require('./dbConnect.js');

var festivalHotelLinkDB = {
	init: init,
	getLinksByHotel: getLinksByHotel,
	addSomeLinks: addSomeLinks,
	saveLinks: saveLinks,
	getLinksByFestival: getLinksByFestival
};

/**
 * here we connect databases to the festivals
 * @returns {review}
 */
var festivalHotelLink = null;
/**
 * module init
 * @returns {*}
 */
function init() {
	try {
		sequelize = DBconnect();
		festivalHotelLink = sequelize.define('festivalHotelLink', {
			'id': {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
			'festivalId': {type: Sequelize.STRING},
			'hotelId': {type: Sequelize.STRING}
		});
		festivalHotelLink.sync(
			// {force: true}
		);
		// addSomeFestivals()
		// addSomeLinks();
		return festivalHotelLink
	} catch (err) {
		console.log('err', err);
	}
}
function getLinksByHotel(hotelId, cb) {
	festivalHotelLink.findAll({
		where: {
			hotelId: hotelId
		}
	}).then(function (data) {
		// console.log(data);
		cb(null, data)
	}, function (err) {
		console.log(err);
	})
}
function getLinksByFestival(festivalId, cb) {
	festivalHotelLink.findAll({
		where: {
			festivalId: festivalId
		}
	}).then(function (data) {
		// console.log(data);
		cb(null, data)
	}, function (err) {
		console.log(err);
	})
}
function saveLinks(data, cb) {
	festivalHotelLink.destroy({where: {hotelId: data[0].hotelId}})
		.then(function () {
			festivalHotelLink.bulkCreate(data)
				.then(function (data) {
					console.log('links created');
					if (!!cb) {
						cb(null, data)
					}
				}, function (err) {
					if (!!cb) {
						cb(err)
					} else {
						console.log(err);
					}
				})
		})
}
function addSomeLinks() {
	saveLinks([{
		hotelId: 'd7037463',
		festivalId: '1'
	}, {
		hotelId: 'd7037463',
		festivalId: '2'
	},
		{
			hotelId: 'd642605',
			festivalId: '1'
		}
	]);


}

module.exports = festivalHotelLinkDB;
