var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var Sequelize = require('sequelize');
var sequelize;
var reviewDB = require('./reviewDB.js');
var hotelDB = require('./hotelDB');
var festivalHotelLinkDB = require('./festivalHotelLinkDB');
var festivalsDB = require('./festivalDB');
var async = require('async');
/**
 * g1010131 - city
 * d642605 - hotel
 * .reviewSelector - selector for review
 */

var mainHost = 'https://www.tripadvisor.com';
// var firstUrl = 'https://www.tripadvisor.com/Hotel_Review-g1010131-d642605-Reviews-Le_Meridien_Ra_Beach_Hotel_Spa-El_Vendrell_Costa_Dorada_Province_of_Tarragona_Cataloni.html';
var review = null;
var hotelId = null;
var cityId = null;
var recordsLimit = 30;
// var offsets = null;
// var recordsSaved = 0;
var tripModule = {
	scrapeOne: scrapeOne,
	scrapeMany: scrapeMany,
	doScraping: doScraping,
	// saveReviews: saveReviews,
	setHotelUrl: setHotelUrl,
	parallelScraping: parallelScraping,
	// getHotelReviews: getHotelReviews,
	init: init,
	insertDummies: insertDummies
};

var languages = {
	en: 'com',
	es: 'es',
	fr: 'fr',
};
function getDomainForLanguage(language){
	var domain;
	if(language != 'en'){
		domain = mainHost.replace('.com', '.' + languages[language] + '')
	}else{
		domain = mainHost
	}
	return domain;
}
/**
 * here we defining Ids of Hotel and City
 * @param url
 */

function setHotelUrl(url) {

	try {
		hotelId = url.match(/-d[0-9]*-/ig)[0].replace(/-/g, '');
		cityId = url.match(/-g[0-9]*-/ig)[0].replace(/-/g, '');
		console.log(hotelId, cityId);
		return {
			hotelId: hotelId,
			cityId: cityId
		}
	} catch (err) {
		console.log(err);
	}
}

/**
 * here we init DB and creating model
 * @returns {review}
 */

/**
 * we need to get offset and urls to the next pages
 * @param $
 * @param offsets
 */
function getOffsets($, offsets) {
	// offsets =;
	try {
		var pager = $('.pageNumbers .pageNum');

		pager.each(function (index, elem) {
			offsets.push({
				offset: cheerio(elem).attr('data-offset'),
				link: cheerio(elem).attr('href')
			})
		});
		console.log('offsets.length', offsets.length);
		// console.log(offsets);
	} catch (err) {
		console.log(err);
	}
}
function setHotelRating(id, $) {
	var hotelStars = $('.rating_rr img').attr('content');
	var hotelName = $('#HEADING').text();
	console.log('hotelName', hotelName, hotelStars);
	hotelDB.updateHotel({
		id: id,
		rating: hotelStars,
		description: hotelName
	})
}
/**
 * here we getting url of hotels and start scraping process
 * @param hotelId
 * @param language
 * @param cb
 */
function doScraping(hotelId, language, cb) {
	console.log('hotelId', hotelId);
	// if(typeof recordsSaved == 'undefined'){
	// 	console.log('defining recordsSaved');
	var recordsSaved = 0;
	var offsets = [];
	// }
	// we need url of reviews page and we assume that it has a .com domain
	hotelDB.getHotelUrl(hotelId)
		.then(function (data) {
			if(language != 'en'){
				data.url = data.url.replace('.com/', '.' + languages[language] + '/')
			}
			if (!!data && !!data.url) {
				// var recordsSaved = 0;
				// clearing old reviews
				reviewDB
					.clearHotelRecords(hotelId, language)
					.then(function (deleted) {
						console.log(deleted);
						scrapeOne(data.url, recordsSaved, language)
							.then(function (data) {

								getOffsets(data.body, offsets);
								setHotelRating(hotelId, data.body);
								if(offsets.length){
									scrapeMany(hotelId, data.recordsSaved, cb, offsets, language, 1);

								}else{
									// we have only single page with reviews
									hotelDB.updateReviewsCount(hotelId, data.recordsSaved);
									cb(data.recordsSaved);
								}
								// console.log('new rec saved', data.recordsSaved, 'offsets:', offsets.length);
							})

					})
			}else{
				console.log('no url found for ID:', hotelId);
			}

		})


}
/**
 * scrapes one page of reviews for specified hotel
 * @param url
 * @param recordsSaved
 * @param language
 */
function scrapeOne(url, recordsSaved, language) {
	// console.log(offsets);
	return new Promise(function (success, fail) {
		request({url: url, method: 'GET'}, function (err, resp, body) {
			// console.log(body);
			var res = [];
			if (err) {
				console.log(err);
			}
			var $ = cheerio.load(body);
			// if (!offsets) {
			// 	getOffsets($)
			// }
			var hotelData = setHotelUrl(url);
			processPage($('.reviewSelector'), res, hotelData, language);
			reviewDB.saveReviews(res)
				.then(function (data) {
					console.log('save success', data.length, recordsSaved);
					recordsSaved += data.length;
					success({
						recordsSaved: recordsSaved,
						body: $
					})
				}, function (err) {
					console.log('save err', err);
					fail()
				});
			// fs.writeFileSync('./test.html', body);
		});
	})
}

/**
 * scraping many records up to the specified limit
 */
function scrapeMany(hotelId, recordsSaved, cb, offsets, language, iteration) {
	if (!!offsets && offsets.length) {
		// var newOffset = reviewDB.getRecordsSaved() / 10;
		console.log('recordsSaved', recordsSaved);
		// var newOffset = recordsSaved / 10;
		// if (reviewDB.getRecordsSaved() < recordsLimit) {
		console.log('iteration', iteration, offsets.length );
		if ((recordsSaved <= recordsLimit) && (offsets.length > iteration)) {
			scrapeOne(getDomainForLanguage(language) + offsets[iteration].link, recordsSaved, language)
				.then(function (data) {
					// console.log('AAAA', data.recordsSaved, 'hotelId', hotelId);
					scrapeMany(hotelId, data.recordsSaved, cb, offsets, language, ++iteration);
				})
		} else {
			console.log('all saved ' + hotelId);
			// hotelDB.updateReviewsCount(hotelId, reviewDB.getRecordsSaved())
			hotelDB.updateReviewsCount(hotelId, recordsSaved);
			// cb(reviewDB.getRecordsSaved());
			cb(recordsSaved);
			reviewDB.resetRecordSaved()
		}
	}
}

function parallelScraping(festivalId, language, final) {
	var arr = [
		// 'd6727310',
		// 'd642605',
		// 'd543153'
	];
	festivalHotelLinkDB.getLinksByFestival(festivalId, function (err, data) {
		// console.log('getLinksByFestival', data);
		arr = data.map(function (elem) {
			return elem.hotelId;
		});
		console.log(arr);
		var scrapes2 = [];
		arr.forEach(function (id) {
			scrapes2.push(function (cb) {
				(function (hotelId) {
					// var recordsSaved = 0;
					doScraping(hotelId, language, function (data) {
						// console.log('hotel id', hotelId);
						console.log('do scraping callback', data);
						cb(null, data)
					})
				})(id)
			})
		});

		async.parallel(scrapes2, function (err, res) {
			console.log('getting to final');
			final(err, res);
			console.log(res);
		})
	});
}
/**
 * here we do parsing of the page
 * @param $partialReview
 * @param res
 * @param hotelData
 */
function processPage($partialReview, res, hotelData, language) {
	$partialReview.each(function (index, elem) {
		try {
			// we can get the amount of stars from the name of the class, i.e. s40
			var stars = cheerio(elem).find('.sprite-rating_s_fill.rating_s_fill').attr('class')
				.replace('sprite-rating_s_fill', '')
				.replace('rating_s_fill', '')
				.replace(/\s*/ig, '')
				.replace('s','');
			// we need to figure out is there more data available and strip off whitespaces and garbage
			var isMoreDataAvailable = false;
			var description = cheerio(elem).find('.entry .partial_entry');
			var addMore = cheerio(description).find('.partnerRvw');
			if (typeof addMore[0] != 'undefined') {
				isMoreDataAvailable = true;
				cheerio(description).find('.partnerRvw').remove();
			}
			description = description.text().replace(/^\s+|\s+$/ig, '');
			// console.log('hotelData', hotelData);
			res.push({
				reviewId: cheerio(elem).attr('id').match(/\d+/ig)[0],
				title: cheerio(elem).find('.noQuotes').text(),
				description: description,
				hotelId: hotelData.hotelId,
				cityId: hotelData.hotelId,
				userImage: cheerio(elem).find('.avatar img').attr('src'),
				userName: cheerio(elem).find('.username span').text(),
				userLocation: cheerio(elem).find('.location').text(),
				ratingDate: cheerio(elem).find('.ratingDate').text(),
				fullReviewUrl: cheerio(elem).find('.quote a').attr('href'),
				stars: stars,
				isMoreDataAvailable: isMoreDataAvailable,
				language: language
			});

		} catch (err) {
			console.log(err);
		}
	});
	console.log('res.length', res.length);
}
/**
 * adds some dummies for festivals, links and hotels
 */
function insertDummies(cb){
	hotelDB.addSomeHotels();
	festivalHotelLinkDB.addSomeLinks();
	festivalsDB.addSomeFestivals();
	cb()
}

/**
 * init module
 * @type {review}
 */
function init(newReview) {
	review = newReview;
}


module.exports = tripModule;