/**
 * Created by Aleksandr Volkov on 23/08/16.
 */
var Sequelize = require('sequelize');
var sequelize;
var fs = require('fs');
var DBconnect = require('./dbConnect.js');
var tripDB = {
	dbInit: dbInit,
	saveReviews: saveReviews,
	clearHotelRecords: clearHotelRecords,
	getHotelReviews: getHotelReviews,
	getRecordsSaved: getRecordsSaved,
	resetRecordSaved: resetRecordSaved,
	getReviewsCount: getReviewsCount
};
var review = null;
var recordsSaved = 0;
/**j
 *
 * here we init DB and creating model
 * @returns {review}
 */
// for mysql DB to support UTF 4bytes characters
// alter database scrape1 character set = utf8mb4 collate = utf8mb4_unicode_ci;
// alter table reviews convert to character set utf8mb4 collate utf8mb4_unicode_ci;
function dbInit() {
	try {
		sequelize = DBconnect();
		review = sequelize.define('review', {
			'id': {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
			'reviewId': {type: Sequelize.STRING,},
			'description': {type: Sequelize.STRING(2048)},
			'title': {type: Sequelize.STRING},
			'userLocation': {type: Sequelize.STRING},
			'hotelId': {type: Sequelize.STRING},
			'userImage': {type: Sequelize.STRING},
			'userName': {type: Sequelize.STRING},
			'fullReviewUrl': {type: Sequelize.STRING},
			'stars': {type: Sequelize.STRING},
			'ratingDate': {type: Sequelize.STRING},
			'language': {type: Sequelize.STRING},
			'isMoreDataAvailable': {type: Sequelize.BOOLEAN}
		});
		review.sync(
			// {force: true}
		);
		return review
	} catch (err) {
		console.log('err', err);
	}
}

/**
 * we need all new data for the hotel evey time we run scraper, so we delete the old ones
 * @param hotelId
 * @param language
 * @returns {*}
 */
function clearHotelRecords(hotelId, language) {
	// console.log('language for deletion', language);
	return review.destroy({where: {hotelId: hotelId, language: language}})
}

/**
 * here we do bulk save of the hotel data
 * @param data
 */
function saveReviews(data) {
	return new Promise(function (success, fail) {
		review
			.bulkCreate(data)
			.then(function (data) {
				// recordsSaved += data.length;
				console.log(data.length, ' records inserted without errors');
				success(data);
			}, function (err) {
				fail(err);
			})
	}, function (err) {
		fail(err);
	});
}
/**
 * retrieving reviews for selected hotel
 */
function getHotelReviews(hotelId, language, cb) {
	// console.log(hotelId);
	review.findAll({where: {hotelId: hotelId, language: language}})
		.then(function (data) {
			cb(data)
		})
}
/**
 * returns amount of saved records
 * @returns {number}
 */
function getRecordsSaved() {
	return recordsSaved;
}
/**
 * we need this data to update amount of reviews
 * @returns {number}
 */
function getReviewsCount(hotelId){
	console.log('getReviewsCount', hotelId);
	return review.count({where: {hotelId: hotelId}});
}
function resetRecordSaved() {
	recordsSaved = 0;
	return recordsSaved;

}

module.exports = tripDB;