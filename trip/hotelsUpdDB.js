/**
 * Created by Aleksandr Volkov on 01/09/16.
 */
var Sequelize = require('sequelize');
var sequelize;
var DBconnect = require('./dbConnect.js');
const month = 1000 * 60 * 60 * 24 * 30;
/**
 * table which stores the date of the last update
 * @constructor
 */
function HotelsUpd() {
	console.log('HotelsUpd init');
	try {
		sequelize = DBconnect();
		this.hotelUpd = sequelize.define('hotelUpd', {
			'id': {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
			'hotelId': {type: Sequelize.STRING},
			'language': {type: Sequelize.STRING},
			'updDate': {type: Sequelize.DATE}
		});
		this.hotelUpd.sync({
			// force: true
		})
	} catch (err) {
		console.log(err);
	}

}
/**
 * removes record with upd time from DB
 * @param hotelId
 * @param language
 * @returns {Promise}
 */
HotelsUpd.prototype.remove = function (hotelId, language) {
	console.log('remove from hotelsUpdDb');
	return new Promise(function (success, fail) {
		this.hotelUpd.destroy({where: {hotelId: hotelId, language: language}})
			.then(function (data) {
				success(data)
			}, function (err) {
				fail(err)
			})
	}.bind(this))
};
/**
 * creating new time upd record
 * @param params
 * @param cb
 */

HotelsUpd.prototype.add = function (params) {
	return new Promise(function (success, fail) {
		this.hotelUpd.create(params)
			.then(function (data) {
				success(data);
			}, function (err) {
				fail(err);
			})

	}.bind(this))
};

HotelsUpd.prototype.getUpd = function (hotelId, language) {
	return new Promise(function (success, fail) {
		this.hotelUpd.find({where: {hotelId: hotelId, language: language}})
			.then(function (data) {
				success(data)
			}, function (err) {
				fail(err)
			})
	}.bind(this))
};
/**
 * getting last time of update for hotel
 * @param hotelId
 * @param language
 * @param cb
 */

HotelsUpd.prototype.getUpdDate = function (hotelId, language, cb) {
	this.hotelUpd.find({where: {hotelId: hotelId, language: language}})
		.then(function (data) {
			cb(null, data);
		}, function (err) {
			cb(err)
		})
};
/**
 * updates time of last access for the hotel and specified language
 * @param hotelId
 * @param language
 * @param updDate
 */
HotelsUpd.prototype.upd = function (hotelId, language, updDate) {
	return new Promise(function (success, fail) {
		this.hotelUpd.update({updDate: updDate}, {where: {hotelId: hotelId, language: language}})
			.then(function (data) {
				success(data)
			}, function (err) {
				console.log(err);
				fail(err)
			})
	}.bind(this))
};
/**
 * checks for the time of the last update
 */
HotelsUpd.prototype.isTimeToScrape = function (hotelId, language) {
	return new Promise(function (success, fail) {
		this.hotelUpd
			.find({where: {hotelId: hotelId, language: language}})
			.then(function (data) {
				// console.log(data.dataValues);
				// no data exist, time to update
				if (!data) {
					console.log('no data found in isTimeToScrape');
					success({isUpdateNeeded: true});
				}
				// we have some data
				if (!!data) {
					// last update was more than month ago
					if ((new Date() - data.updDate) > month) {
						console.log('we need to update', data.updDate);
						success({isUpdateNeeded: true})
					} else {
						console.log('we don\'t need to updated', data.updDate);
						success({isUpdateNeeded: false})
					}
				}
				// data exist
			}, function (err) {
				fail(err)
			})

	}.bind(this))
};

module.exports = HotelsUpd;