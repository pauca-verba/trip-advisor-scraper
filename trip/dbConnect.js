/**
 * Created by Aleksandr Volkov on 31/08/16.
 */
var Sequelize = require('sequelize');
var sequelize;


module.exports = function () {
	if (!sequelize) {
		try {
			console.log('new connection to DB');
			if(process.env.sqlite){
				sequelize = new Sequelize('database', 'user', 'pass', {
					dialect: 'sqlite',
					storage: './trip.sqlite3',
					logging: false,
					charset: 'utf8',
					collate: 'utf8_general_ci'
				});
			}else{
				console.log('trying to connect mysql');
				sequelize = new Sequelize('scrape1', 'root', 'zrT6gZpt', {
					dialect: 'mysql',
					storage: './trip.sqlite3',
					logging: false,
					charset: 'utf8',
					// collate: 'utf8',
					dialectOptions: {
						charset: 'utf8mb4'
					}
				});
			}
			return sequelize;
		} catch (err) {
			console.log(err);
		}
	}else{
		return sequelize;
	}

};