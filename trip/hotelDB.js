/**
 * Created by Aleksandr Volkov on 23/08/16.
 */
var Sequelize = require('sequelize');
var sequelize;
var DBconnect = require('./dbConnect.js');

var festivalHotelLinkDB = require('./festivalHotelLinkDB.js');
var reviewDB = require('./reviewDB.js');
var festivalHotelLink = festivalHotelLinkDB.init();
var hotelsUpdDB = new require('./hotelsUpdDB.js')();
var hotelDB = {
	init: init,
	addSomeHotels: addSomeHotels,
	saveHotel: saveHotel,
	getHotels: getHotels,
	getHotelUrl: getHotelUrl,
	updateReviewsCount: updateReviewsCount,
	removeHotel: removeHotel,
	getHotel: getHotel,
	updateHotel: updateHotel
};

/**
 * here we init DB and creating model
 * @returns {review}
 */
var hotel = null;
/**
 * module init
 * @returns {*}
 */
function init() {
	try {
		sequelize = DBconnect();
		hotel = sequelize.define('hotel', {
			'id': {type: Sequelize.STRING, primaryKey: true},
			'description': {type: Sequelize.STRING},
			'url': {type: Sequelize.STRING, unique: true},
			'reviewsCount': {type: Sequelize.INTEGER},
			'rating': {type: Sequelize.STRING}
		});
		hotel.sync(
			// {force: true}
		);
		// addSomeHotels()
		return hotel
	} catch (err) {
		console.log('err', err);
	}
}
/**
 * after scraping we need to update amount of scraped records
 * @param hotelId
 * @param reviewsCount
 */
function updateReviewsCount(hotelId, reviewsCount) {
	console.log('updateReviewsCount', hotelId, reviewsCount);
	reviewDB.getReviewsCount(hotelId)
		.then(function (data) {
			console.log('reviews count', data);
			hotel.update({reviewsCount: data}, {
				where: {id: hotelId}
			}).then(function () {
				console.log('reviews count update succeed');
			}, function (err) {
				console.log(err);
			})

		});
}
/**
 * links to the festivals needs to be updated after Hotel edit
 * @param hotelData
 * @param cb
 */
function updateLinks(hotelData, cb) {
	if (!!hotelData.festivals) {
		var links = hotelData.festivals.map(function (elem) {
			return {
				hotelId: hotelData.id,
				festivalId: elem.festivalId
			}
		});
		console.log(links);
		festivalHotelLinkDB.saveLinks(links);

	}
}
/**
 * saving hotels
 * @param hotelData
 * @param cb callback after data successfully saved
 */
function saveHotel(hotelData, cb) {
	console.log(hotelData);
	try {
		// we need to save only domain in .com zone
		hotelData.url = hotelData.url.replace(/www\.tripadvisor\.([a-z])+\//ig, 'www.tripadvisor.com/' );
		if (!hotelData.id) {
			hotelData.id = hotelData.url.match(/-d[0-9]*-/ig);
			if (!!hotelData.id)
				hotelData.id = hotelData.id[0].replace(/-/g, '');
		}
		if (!hotelData.description) {
			hotelData.description = hotelData.url.match(/Reviews-.+\.html$/ig);
			if (!!hotelData.description) {
				hotelData.description = hotelData.description[0].replace(/Reviews-/ig, '').replace('.html', '')
			}
		}
		if (!!hotelData.id) {
			hotel.create(hotelData).then(function (data) {
				updateLinks(hotelData);
				if (!!cb) {
					cb(null, data)
				}
				console.log(hotelData.description);
			}, function (err) {
				if (!!cb) {
					cb(err)
				}
				console.log(err);
			})
		} else {
			cb({message: 'id not specified or url is not correct'})
		}
	} catch (err) {
		console.log(err);
	}

}
/**
 * removing hotel from DB
 * @param hotelId
 * @param cb
 */
function removeHotel(hotelId, cb) {
	hotel.destroy({where: {id: hotelId}})
		.then(function (data) {
			console.log(data);
			cb(null, data)

		}, function (err) {
			console.log(err);
			cb(err)
		})
}
/**
 * updating hotel data
 * @param hotelData
 * @param cb
 */
function updateHotel(hotelData, cb) {
	// console.log(hotelData);
	hotel.update(hotelData, {where: {id: hotelData.id}})
		.then(function (data) {
			if (!!hotelData.festivals) {
				updateLinks(hotelData, cb)
				if (!!cb) {
					cb(null, data)
				}
			} else {
				if (!!cb) cb(null, data)

			}
			// console.log(hotelData.description);
		}, function (err) {
			if (!!cb) {
				cb(err)
			}
			console.log(err);
		})
}
/**
 * returns promise with hotel data
 * @param hotelId
 * @returns {*}
 */
function getHotelUrl(hotelId) {
	return hotel.findById(hotelId)
}
/**
 * get all the hotels
 * @param cb
 * @param festivalId
 */
function getHotels(festivalId, cb) {
	// console.log('fest id', festivalId);
	if (!!festivalId) {

		sequelize.query('select h.id, h.description, h.url, h.rating, h.reviewsCount from hotels h, festivalHotelLinks ' +
			'l where l.festivalId = ? and l.hotelId = h.id;', {replacements: [festivalId]})
			.then(function (data) {
				// console.log(data);
				cb(data[0])
			}, function (err) {
				console.log(err);
			});
	} else {
		// if we want to see all the hotels
		hotel.findAll()
			.then(function (data) {
				cb(data)
			}, function (err) {
				console.log(err);
			})
	}

}
/**
 * runs a callback after on receive data about hotel
 * @param hotelId
 * @param cb
 * @param language
 */
function getHotel(hotelId, cb, language) {
	getHotelUrl(hotelId)
		.then(function (hotelData) {
			festivalHotelLinkDB.getLinksByHotel(hotelId, function (err, links) {
				// console.log('links ', links, hotelId);
				if (!!hotelData) {
					hotelData.dataValues.links = links;
				}
				if (!!language) {
					// hotelsUpdDB.getUpdDate(hotelId, language, function(err, updData){
					// 	if(err){
					// 		console.log(err);
					// 	}else{
					// 		// no records exists
					// 		if(!updData.length){
					// 			hotelData.needScraping = true;
					// 		}else{
					// 			// month
					// 			var month = 1000 * 60 * 60 * 24 * 30;
					// 			new Date(hotelData.updDate) - month;
					// 			new Date(d.getTime() - 1000 * 60 * 60 * 24 * 30)
					// 		}
					// 	}
					//
					// })


				} else {
					cb(hotelData);
				}
				// console.log(links);
			})
		}, function (err) {
			console.log(err);
		})

}
/**
 * some test data, needs to be removed
 */
function addSomeHotels() {
	saveHotel({
		id: 'd642605',
		url: 'https://www.tripadvisor.com/Hotel_Review-g1010131-d642605-Reviews-Le_Meridien_Ra_Beach_Hotel_Spa-El_Vendrell_Costa_Dorada_Province_of_Tarragona_Cataloni.html',
		description: 'Le_Meridien_Ra_Beach_Hotel_Spa'
	});
	saveHotel({
		id: 'd6727310',
		url: 'https://www.tripadvisor.com/Hotel_Review-g60763-d6727310-Reviews-The_Ludlow_New_York_City-New_York_City_New_York.html',
		description: 'The_Ludlow_New_York_City-New_York_City_New_York'
	});
	saveHotel({
		id: 'd7037463',
		url: 'https://www.tripadvisor.com/Hotel_Review-g294212-d7037463-Reviews-NUO_Hotel_Beijing-Beijing.html',
		description: 'NUO_Hotel_Beijing-Beijing'
	});
	saveHotel({
		id: 'd543153',
		url: 'https://www.tripadvisor.com/Hotel_Review-g187497-d543153-Reviews-Casa_Camper_Hotel_Barcelona-Barcelona_Catalonia.html',
		description: 'Casa_Camper_Hotel_Barcelona'
	});
}

module.exports = hotelDB;