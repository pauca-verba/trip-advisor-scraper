/**
 * Created by Aleksandr Volkov on 25/08/16.
 */
var Sequelize = require('sequelize');
var sequelize;
var DBconnect = require('./dbConnect.js');
var festivalDB = {
	init: init,
	addSomeFestivals: addSomeFestivals,
	createFestival: createFestival,
	getFestivals: getFestivals,
	updateFestival: updateFestival,
	removeFestival: removeFestival
};

/**
 * here we init DB and creating model
 * @returns {review}
 */
var festival = null;
/**
 * module initj
 * @returns {*}
 */
function init() {
	try {
		sequelize = DBconnect();
		festival = sequelize.define('festival', {
			'id': {type: Sequelize.STRING, primaryKey: true},
			'description': {type: Sequelize.STRING},
			'hotelCount': {type: Sequelize.INTEGER, defaultValue: 0}
		});
		festival.sync(
			// {force: true}
		);
		// addSomeFestivals()
		return festival
	} catch (err) {
		console.log('err', err);
	}
}
/**
 * list of all festivals
 * @param cb
 */
function getFestivals(cb) {
	festival.findAll()
		.then(function (data) {
			cb(null, data)
		}, function (err) {
			console.log(err);
			cb(err)
		})
}
/**
 * new festival creation
 * @param data
 */
function createFestival(data) {
	festival.create(data)
		.then(function (data) {
			console.log('festival created');
		}, function (err) {
			console.log(err);
		})
}
/**
 * update festival
 * @param data
 */
function updateFestival(data) {
	festival.update({where: {id: data.id}}, data)
		.then(function (data) {
			console.log('festival updated');
		}, function (err) {
			console.log(err);
		})
}
/**
 * remove festival
 */
function removeFestival(festivalId) {
	festival.destroy({where: {id: festivalId}})
		.then(function (data) {
			console.log('festival removed', festivalId);
		}, function (err) {
			console.log(err);
		})
}
/**
 * dummy data for festival
 */
function addSomeFestivals() {
	createFestival({
		id: 1,
		description: 'Festival 1'
	});


	createFestival({
		id: 2,
		description: 'Festival 2'
	});

	createFestival({
		id: 3,
		description: 'Festival 3'
	});

}

module.exports = festivalDB;
